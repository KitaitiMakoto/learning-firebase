Learning Firebase
=================

Learning Firebase by GodeGrid's [一から始めるFirebase][] and [Get Started: Write and Deploy Your First Functions][].

[一から始めるFirebase]: https://app.codegrid.net/series/2017-firebase-basic
[Get Started: Write and Deploy Your First Functions]: https://firebase.google.com/docs/functions/get-started
