document.addEventListener('DOMContentLoaded', () => {
  var $signin = document.querySelector('#signin');
  var provider = new firebase.auth.GoogleAuthProvider();

  $signin.addEventListener('click', function() {
    firebase.auth().signInWithRedirect(provider);
  });

  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      var $profile = document.querySelector('#profile');
      $profile.innerHTML = `
        <div>uid: ${user['uid']}</div>
        <div>displayName: ${user['displayName']}</div>
        <div>email: ${user['email']}</div>
        <img src="${user['photoURL']}" width="100">
      `
    }
  });

  var $signout = document.querySelector('#signout');

  $signout.addEventListener('click', function() {
    firebase.auth().signOut().then(function() {
      location.reload();
    });
  });

  const storage = firebase.storage();
  const imagesStorageRef = storage.ref('images');

  const db = firebase.database();
  const imagesRef = db.ref('/images');

  let $file = document.querySelector('#file');

  $file.addEventListener('change', event => {
    let files = event.target.files;
    Array.from(files).forEach(upload);

  });

  function upload(file) {
    return imagesRef.push().then(newImageRef => {
      return imagesStorageRef.child(newImageRef.key).put(file).then(fileInfo => {
        console.log(fileInfo);
      });
    }).catch(error => console.error(error));
  }
});
