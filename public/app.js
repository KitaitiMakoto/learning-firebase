document.addEventListener('DOMContentLoaded', function() {
  var $input = document.querySelector('#input');
  var $output = document.querySelector('#output');

  var db = firebase.database();
  var messageRef = db.ref('/message');

  $input.addEventListener('input', function(e) {
    var target = e.target;
    messageRef.set(target.value);
  });

  messageRef.on('value', function(snapshot) {
    $output.textContent = snapshot.val();
  });

  var $signin = document.querySelector('#signin');
  var provider = new firebase.auth.GoogleAuthProvider();

  $signin.addEventListener('click', function() {
    firebase.auth().signInWithRedirect(provider);
  });

  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      var $profile = document.querySelector('#profile');
      $profile.innerHTML = `
        <div>uid: ${user['uid']}</div>
        <div>displayName: ${user['displayName']}</div>
        <div>email: ${user['email']}</div>
        <img src="${user['photoURL']}" width="100">
      `
    }
  });

  var $signout = document.querySelector('#signout');

  $signout.addEventListener('click', function() {
    firebase.auth().signOut().then(function() {
      location.reload();
    });
  });

  var arrayRef = db.ref('/array');
  var array = ['A', 'B', 'C', 'D', 'E'];
  arrayRef.set(array);

  arrayRef.on('value', function(snapshot) {
    var value = snapshot.val();
    console.log(value);
  });

  // arrayRef.child('0').remove();
  // arrayRef.child('1').remove();
  // arrayRef.child('3').remove();

  // arrayRef.child('0').remove();
  // arrayRef.child('2').set('CC');
  // console.log(arrayRef.child('4'));

  var $title = document.querySelector('#title');
  var $body = document.querySelector('#body');
  var $date = document.querySelector('#date');
  var $tags = document.querySelector('#tags');
  var $addBtn = document.querySelector('#add-btn');
  var todosRef = db.ref('/todos');
  $addBtn.addEventListener('click', function() {
    var tags = {};
    $tags.value.split(',').forEach(function(tag) {
      tags[tag] = true;
    });
    var data = {
      title: $title.value,
      body: $body.value,
      complete: false,
      date: $date.value,
      tags: tags
    };
    todosRef.push(data).then(function(item) {
      Object.keys(tags).forEach(function(tag) {
        db.ref(`/tags/${tag}`).child(item.key).set(true);
      });
      db.ref('/uncompleted').child(item.key).set(true);
    });
  });
});
